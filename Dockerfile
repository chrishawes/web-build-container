FROM alpine:latest
MAINTAINER chrishawes
ENV COMPOSER_VERSION 1.0.0-alpha11

RUN apk add curl php7 php7-json php7-phar php7-openssl php7-pcntl php7-zip php7-mcrypt php7-opcache php7-bz2 php7-common php7-xdebug nodejs --update-cache --repository http://dl-3.alpinelinux.org/alpine/edge/testing/ --allow-untrusted && rm -rf /var/cache/apk/*
COPY php.ini /etc/php7/php.ini
RUN ln -s /usr/bin/php7 /usr/bin/php
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --version=${COMPOSER_VERSION}
